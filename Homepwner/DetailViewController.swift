//
//  DetailViewController.swift
//  Homepwner
//
//  Created by Jay on 27/08/17.
//  Copyright © 2017 the chalakas. All rights reserved.
//

import UIKit

//this is to show the the details page, and fill it with details

//added UITEXTFIELDDELEGATE is for allowing the enter key to make the keyboard dissapear

class DetailViewController : UIViewController, UITextFieldDelegate
{
    //this method will work with the Tap Gesture Recognizer to disable keyboard when any area is tappe
    //this works in conjunction with the pressing Enter that also does the same thing
    @IBAction func backgroundTapped(_ sender: UITapGestureRecognizer)
    {
        view.endEditing(true)
    }
    //just an update, I am not using a number formatter as per the book.
    //TODO : user number formatter Page number 236
    //TODO : Serial Number is not showing
    
    @IBOutlet var dateLabel: UILabel!
    @IBOutlet var nameField: UITextField!
    @IBOutlet var serialNumberField: UITextField!
    @IBOutlet var valueField: UITextField!
    
    //automatically set the name of the page details
    var item: Item! { didSet { navigationItem.title = item.name }}
    
    
    
    //this loads up when the view shows up
    override func viewWillAppear(_ animated: Bool)
    {
        super.viewWillAppear(animated)
        
        nameField.text = item.name
        serialNumberField.text = item.serialNumber
        valueField.text = "\(item.valueInDollars)"
        dateLabel.text = "\(item.dateCreated)"
    }
    
    //I want to save values entered. 
    
    override func viewWillDisappear(_ animated: Bool)
    {
        super.viewWillDisappear(animated)
        
        //hide the keyboard if it is open when back button is pressed
        view.endEditing(true)
        
        //put all the values from the page into the object
        item.name = nameField.text!
        //TODO update dollar value as well. this is linked to the number formatter. Page number 246
        
    }
    
    //this is for the keyboard dissapearing thing
    func textFieldShouldReturn(_ textField: UITextField) -> Bool
    {
        textField.resignFirstResponder()
        return true
    }
}

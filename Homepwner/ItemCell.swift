//
//  ItemCell.swift
//  Homepwner
//
//  Created by Jay on 27/08/17.
//  Copyright © 2017 the chalakas. All rights reserved.
//

import UIKit

//Class that will be used to show more details inside a cell. 

class ItemCell : UITableViewCell
{
    //outlets that connect to the labels
    @IBOutlet var nameLabel: UILabel!
    @IBOutlet var serialNumberLabel: UILabel!
    @IBOutlet var valueLabel: UILabel!
    
    //this will ensure that the text changes will respond immediatly
    override func awakeFromNib()
    {
        super.awakeFromNib()
        
        nameLabel.adjustsFontForContentSizeCategory = true
        serialNumberLabel.adjustsFontForContentSizeCategory = true
        detailTextLabel?.adjustsFontForContentSizeCategory = true
    }
    
}

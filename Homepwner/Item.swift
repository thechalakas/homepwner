//
//  Item.swift
//  Homepwner
//
//  Created by Jay on 07/08/17.
//  Copyright © 2017 the chalakas. All rights reserved.
//

import UIKit

//this is the item that will used by the table view to show everything
//name of the item being pawned
//dollar value
//serial number
//date that was used to create this thing

class Item: NSObject
{
    var name:String
    var valueInDollars: Int
    var serialNumber: String?  //this one is optional, hence the question mark
    let dateCreated: Date
    
    //this is the designated initializer - kind of like the constructors i think - for the item
    //these are mandatory, and there should at least be one, especially since the properties do not have a default
    //value
    
    
    init(name:String,serialNumber: String?, valueInDollars: Int)
    {
        self.name = name
        self.valueInDollars = valueInDollars
        self.serialNumber = serialNumber
        self.dateCreated = Date()
        
        super.init()
    }
    
    //the following is a convenience initializer, which is, a convenience and hence optional
    
    convenience init(random: Bool = false)
    {
    
        //the parameter will be a true or false
        //if true, then we create an Item with randomly assigned values
        if random
        {
            //the entire logic used in this full if section is to simply generate random names, serial numbers and dollar values
            
            //collection of adjectives and nouns
            
            let adjectives = ["Fluffy","Rusty","Shiny"]
            let nouns = [ "Bear","Spork","Mac"]
            
            //creating a random adjective
            var idx = arc4random_uniform(UInt32(adjectives.count))
            let randomAdjective = adjectives[Int(idx)]
            
            
            //creating a random noun
            idx = arc4random_uniform(UInt32(nouns.count))
            let randomNoun = nouns[Int(idx)]
            
            //creating a random name by concatenating the 2 random noun and adjective things
            
            let randomName = "\(randomAdjective) \(randomNoun)"
            
            
            //we got a name, now the remaining 2 things.
            let randomValue = Int(arc4random_uniform(100))
            let randomSerialNumber = UUID().uuidString.components(separatedBy: "-").first!
            
            //alright, we got all three things
            //lets INITIALIZE!!
            //we are calling the designated initializer from above
            self.init(name: randomName, serialNumber: randomSerialNumber, valueInDollars: randomValue)
            
        }
        else
        {
            //we are calling the designated initializer from above
            self.init(name:"",serialNumber:nil,valueInDollars:0)
        }
        
    }
}

























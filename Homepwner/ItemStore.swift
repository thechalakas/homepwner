//
//  ItemStore.swift
//  Homepwner
//
//  Created by Jay on 07/08/17.
//  Copyright © 2017 the chalakas. All rights reserved.
//

import UIKit

//this is to store the collection of items
class ItemStore
{
    //array of Items
    var allItems = [Item]()
    
    
    //create a random item
    @discardableResult func createItem() -> Item
    {
        //creating a new Item
        let newItem = Item(random: true)
        //add the Item to the collection
        allItems.append((newItem))
        //return the Item
        //of course, this method has the discardable Result thing, so return values can be ignored when called
        return newItem
    }
    
    //initialize the collection with 5 items
    
    init()
    {
        for _ in 0..<5
        {
            createItem()
        }
    }
    
    //delete an item
    func removeItem(_ item: Item)
    {
        let index = allItems.index(of:item)
        if (index != nil)
        {
            allItems.remove(at: index!)
        }
    }
    
    //move item within the collection 
    func moveItem(from fromIndex: Int, to toIndex: Int)
    {
        //if they are same, from and to, dont do anything
        if(fromIndex == toIndex)
        {
                return
        }
        
        //get the item
        let movedItem = allItems[fromIndex]
        //remove it from collection
        allItems.remove(at: fromIndex)
        //insert that item into the new location
        allItems.insert(movedItem, at: toIndex)
    }
    
    
}

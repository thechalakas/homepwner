//
//  ItemsViewController.swift
//  Homepwner
//
//  Created by Jay on 07/08/17.
//  Copyright © 2017 the chalakas. All rights reserved.
//

import UIKit


class ItemsViewController: UITableViewController
{
    //alright, we have a store class and Item class
    //lets use it here in the controller which will in turn feed it to the view
    
    var itemStore: ItemStore!
    
    //okay, so we have teh ItemStore. Lets plug that to the UIView
    //Remember the file in which you are reading this is the C of MVC. 
    //the ItemStore is the M
    //so, naturally, we need to look at the V now.
    
    //this is to get the edit button right next to the title of the navigation bar at the top. 
    
    required init?(coder aDecoder: NSCoder)
    {
        super.init(coder: aDecoder)
        navigationItem.leftBarButtonItem = editButtonItem
    }
    
    //this is to be used with add new item mode
    //okay, changing this from a button to a UIBARBUTTON
    @IBAction func addNewItem(_ sender: UIBarButtonItem)
    {
        
        //first we must add an item to data store
        let newItem = itemStore.createItem()
        
        //then add the item to the view
        //first we need the position of the new item in the array
        let index = itemStore.allItems.index(of: newItem)
        
        //if index is true, then we add it to the view
        if (index != nil)
        {
            //get the path
            let indexpath = IndexPath(row: index!, section: 0)
            //add it to the View row
            tableView.insertRows(at: [indexpath], with: .automatic)
        }
    }
    
    //this is for editing stuff
    //this method is no longer used. Just leaving it in
    @IBAction func toggleEditingMode(_ sender: UIButton)
    {
        //if currently editing, then on button press you will have the words 'done' displayed. change it to 'Edit'
        //also, set the state to not editing.
        if isEditing
        {
            sender.setTitle("Edit", for: .normal)
            setEditing(false, animated: true)
        }
        //if not editing, then on button press you will see the words edit, change it to done.
            //chenge the state to editing
        else
        {
            sender.setTitle("Done", for: .normal)
            setEditing(true, animated: true)
        }
    }
    
    //alright we need override the built in delete method of the table UI
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath)
    {
        //lets remove the item from the store
        if editingStyle == .delete
        {
            //collect the item
            let item = itemStore.allItems[indexPath.row]
            
            //now, let put on some display alerts
            
            let title = "Delete \(item.name)"
            
            let message = "Are you sure you want to delete this item. Once you do this, it is gone!"
            
            //lets create the alert controller
            let ac = UIAlertController(title: title, message: message, preferredStyle: .actionSheet)
            
            //now, lets link the Cancel and Delete pressess
            
            //canel means, no handler because there is nothing to do
            let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
            ac.addAction(cancelAction)
            
            //this is for the delete action
            let deleteAction = UIAlertAction(title: "Delete", style: .destructive, handler:
            {
                (action) -> Void in
                
                //remove the item from the collection
                self.itemStore.removeItem(item)
                
                //and of coure, remove item from the view with animation
                self.tableView.deleteRows(at: [indexPath], with: .automatic)
                
            })
            ac.addAction(deleteAction)
            
            //alright, the AC is better now. Lets present it to the screen
            present(ac, animated: true, completion: nil)
            
        }
        

    }
    
    //alright, we need to override the built in move method
    override func tableView(_ tableView: UITableView, moveRowAt sourceIndexPath: IndexPath, to destinationIndexPath: IndexPath)
    {
        itemStore.moveItem(from: sourceIndexPath.row, to: destinationIndexPath.row)
    }
    
    //sets the number of rows for the table that needs to be displayed by the view
    //this row length indicator is a manadatory implementation for table to work
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return itemStore.allItems.count
    }
    
    //now we need to set what each cell will show
    //this show cell details is also mandatory for table to work
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        //lets define the cell type
        //this creates a new cell each time something new row has to be displayed
        //that means a huge memory hit.
        //let cell = UITableViewCell(style: .value1, reuseIdentifier: "UITableViewCell")
        
        //to avoid the above mentioned memory hit, lets reuse rows
        //comment this default cell
        //let cell = tableView.dequeueReusableCell(withIdentifier: "UITableViewCell", for: indexPath)
        //lets use custom cell
        let cell = tableView.dequeueReusableCell(withIdentifier: "ItemCell", for: indexPath) as! ItemCell
        
        //lets get the item
        let item = itemStore.allItems[indexPath.row]
        
        //now, lets tell what each cell to show
        //commenting the next 2, so we can use custom cell
        //cell.textLabel?.text = item.name
        //cell.detailTextLabel?.text = "$\(item.valueInDollars)"
        //using the custom cell
        cell.nameLabel.text = item.name
        cell.serialNumberLabel.text = item.serialNumber
        cell.valueLabel.text = "$\(item.valueInDollars)"
        
        return cell
    }
    
    //okay, do you see that the table is displayed. however, as it is usual with UI things, they go under the
    //status bar
    //fixing it here
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        //first up, lets get the height of the status bar
        
        //alright, we dont need the height and inset related lines (total 4) anymore.
        //the navigation controller will take care of height and stuff
        //let statusBarHeight = UIApplication.shared.statusBarFrame.height
        
        //let insets = UIEdgeInsets(top: statusBarHeight, left: 0, bottom: 0, right: 0)
        //tableView.contentInset = insets
        //tableView.scrollIndicatorInsets = insets
        
        //lets set the row height, now that we are using custom views. 
        //tableView.rowHeight = 65 //we dont want static heights
        //make the height dynamic
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.estimatedRowHeight = 65
    }
    
    //this method is for sending things from the table view to the details view
    //We are using Segue thing. 
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?)
    {
        //we need to switch between multiple segues that was going out. 
        
        //right now, I only have one. showItem (check out the arrow)
        switch  segue.identifier
        {
        case "showItem"?:
            //lets find the row that was tapped
            if let row = tableView.indexPathForSelectedRow?.row
            {
                //lets get the item that belongs to the row
                let item = itemStore.allItems[row]
                
                //send the item to the controller
                let detailViewController = segue.destination as! DetailViewController
                detailViewController.item = item
            }
        default:
            preconditionFailure("Unexpected segue identifier")
        }
    }
    
    //need to load data that is coming from 
    override func viewWillAppear(_ animated: Bool)
    {
        super.viewWillAppear(animated)
        
        tableView.reloadData()
    }
    
}




































